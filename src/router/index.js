import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
	{
		path: '/intro',
		component: () => import('@/components/intro.vue')
	},
	{
		path: '/geometry',
		component: () => import('@/components/geometry.vue')
	},
	{
		path: '/texture',
		component: () => import('@/components/texture.vue')
	},
]

const router = new VueRouter({
	routes
})

export default router